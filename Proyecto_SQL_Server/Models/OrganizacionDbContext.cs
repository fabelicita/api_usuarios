﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_SQL_Server.Models
{
    public class OrganizacionDbContext : DbContext
    {

        public OrganizacionDbContext(DbContextOptions<OrganizacionDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuarios>().ToTable("Usuarios");
            modelBuilder.Entity<Organizaciones>().ToTable("Organizaciones");
        }

        public DbSet<Usuarios> Usuarios { get; set; }
        public DbSet<Organizaciones> Organizaciones { get; set; }

    }
}
