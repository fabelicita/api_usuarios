﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_SQL_Server.Models
{
    public class Usuarios
    {
        public int Id { get; set; }
        public string Nombre_Completo { get; set; }
        public string Cedula { get; set; }
        public DateTime Fecha_Nacimiento { get; set; }
        public string Correo_Electronico { get; set; }
        public string Telefono { get; set; }

        [ForeignKey("Organizaciones")]
        public int Organizacion { get; set; }
        public Organizaciones Organizaciones { get; set; }
    }
}
