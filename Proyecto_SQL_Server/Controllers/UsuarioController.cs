﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Proyecto_SQL_Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_SQL_Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {

        private readonly OrganizacionDbContext _context;

        public UsuarioController(OrganizacionDbContext context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Usuarios>>> GetUsuarios()
        {
            return await _context.Usuarios
                .Include(a => a.Organizaciones)
                .ToListAsync();
        }


        // GET: api/Usuario/1
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuarios>> GetUsuario(int id)
        {
            var usuario = await _context.Usuarios.Where(x => x.Id == id)
                .Include(a => a.Organizaciones)
                .FirstOrDefaultAsync();


            if (usuario == null)
            {
                return NotFound();
            }

            return usuario;
        }

        // POST: api/Usuario
        [HttpPost]
        public async Task<ActionResult<Usuarios>> PostUsuario(Usuarios usuario)
        {

            var usuario_registrado = await _context.Usuarios
            .Where(x => x.Cedula == usuario.Cedula || x.Correo_Electronico == usuario.Correo_Electronico || x.Telefono == usuario.Telefono)
            .FirstOrDefaultAsync();

            if (usuario_registrado != null)
            {
                return Problem(statusCode:400, title: "El dato ya esta registrado (Cedula, Correo Electronico o Telefono)");

            }

            try
            {
                _context.Usuarios.Add(usuario);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetUsuario", new { id = usuario.Id }, usuario);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }


        }

        // PUT: api/Usuario/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuario(int id, Usuarios usuario)
        {
            if (id != usuario.Id)
            {
                return BadRequest();
            }

            var usuario_registrado = await _context.Usuarios
            .Where(x => x.Id != usuario.Id && (x.Cedula == usuario.Cedula || x.Correo_Electronico == usuario.Correo_Electronico || x.Telefono == usuario.Telefono))
            .ToListAsync();

            if (usuario_registrado.Count != 0)
            {
                return Problem(statusCode: 400, title: "El dato ya esta registrado (Cedula, Correo Electronico o Telefono)");

            }

            try
            {
                _context.Entry(usuario).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        // DELETE: api/Usuario/1
        [HttpDelete("{id}")]
        public async Task<ActionResult<Usuarios>> DeleteUsuario(int id)
        {
            var usuario = await _context.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            _context.Usuarios.Remove(usuario);
            await _context.SaveChangesAsync();

            return usuario;
        }

        private bool UsuarioExists(int id)
        {
            return _context.Usuarios.Any(e => e.Id == id);
        }

    }
}
